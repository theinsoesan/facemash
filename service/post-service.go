package service

import (
	"fmt"
	"log"

	"github.com/mashingan/smapping"
	"github.com/theinsoesan/app/dto"
	"github.com/theinsoesan/app/entity"
	"github.com/theinsoesan/app/repository"
)

// postService is a ....
type PostService interface {
	Insert(p dto.PostCreateDTO) entity.Post
	Update(p dto.PostUpdateDTO) entity.Post
	Delete(p entity.Post)
	All() []entity.Post
	FindByID(postID uint64) entity.Post
	IsAllowedToEdit(userID string, postID uint64) bool
}

type postService struct {
	postRepository repository.PostRepository
}

// NewpostService .....
func NewPostService(postRepo repository.PostRepository) PostService {
	return &postService{
		postRepository: postRepo,
	}
}

func (service *postService) Insert(p dto.PostCreateDTO) entity.Post {
	post := entity.Post{}
	err := smapping.FillStruct(&post, smapping.MapFields(&p))
	if err != nil {
		log.Fatalf("Failed map %v: ", err)
	}
	res := service.postRepository.InsertPost(post)
	return res
}

func (service *postService) Update(p dto.PostUpdateDTO) entity.Post {
	post := entity.Post{}
	err := smapping.FillStruct(&post, smapping.MapFields(&p))
	if err != nil {
		log.Fatalf("Failed map %v: ", err)
	}
	res := service.postRepository.UpdatePost(post)
	return res
}

func (service *postService) Delete(p entity.Post) {
	service.postRepository.DeletePost(p)
}

func (service *postService) All() []entity.Post {
	return service.postRepository.AllPost()
}

func (service *postService) FindByID(postID uint64) entity.Post {
	return service.postRepository.FindPostByID(postID)
}

func (service *postService) IsAllowedToEdit(userID string, postID uint64) bool {
	p := service.postRepository.FindPostByID(postID)
	id := fmt.Sprintf("%v", p.UserID)
	return userID == id
}
